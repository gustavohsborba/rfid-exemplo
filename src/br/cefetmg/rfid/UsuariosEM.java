/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.cefetmg.rfid;


import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

/**
 *
 * @author gustavo
 */
public class UsuariosEM {
    
    private static EntityManager em;
    private static EntityManagerFactory emf;
    
    private static List<Usuarios> usuarios;
    
    private static Integer proximoID() {
        int maior=0;
        for(Usuarios p : usuarios)
            if(p.getId()>=maior)
                maior = p.getId();
        return maior+1;
    }
    
    
    public static Usuarios procurarPorRFID(String rfid) {
        if(rfid==null) return null;
        for(Usuarios u : usuarios)
            if(u.getRfid().contentEquals(rfid))
                return u;
        
        return null;
    }
    
    public static Usuarios cadastrarUsuario(Usuarios u) throws NegocioException {
        validaUsuario(u);
        
        u.setId(UsuariosEM.proximoID());
        usuarios.add(u);
        return u;
    }
    
     public static Usuarios removerUsuario(Usuarios u) {
        if(usuarios.contains(u))
            usuarios.remove(u);
        else return null;
        return u;
     }
     
     public static Usuarios alterarUsuario(Usuarios u) throws NegocioException {
        validaUsuario(u);
        
        int indice = usuarios.indexOf(u);
        if(indice<=0) return null;
        
        usuarios.remove(indice);
        usuarios.add(u);
        
        return u;
     }
    
    public static Usuarios fazerLogin(Usuarios u) {
        if(usuarios.contains(u))
            return u;
        else return null;
    }
     
     
    
    public static void init(EntityManagerFactory ef, EntityManager e){
        em = e;
        emf= ef;
        usuarios = new ArrayList<Usuarios>();
        usuarios.add(new Usuarios(1, "Gustavo", "123456", "/home/gustavo/Pictures/G.jpg", "5D682A35", 1));
    }
    
    public static void end(){
        em.close();
        emf.close();
    }
    
    
    
    
    
    
    // Tentativas frustradas de usar mySQL:
    
    public static void persisteUsuario( Usuarios u ){
        em.getTransaction().begin();
        em.persist(u);
        em.getTransaction().commit();
    }
    
    public static void mergeUsuario(Usuarios u) {
            
        
    }
    
    
    public static Usuarios buscaUsuarioPorRFID( Usuarios u ){
        if(u==null)
            return u;
        
        em.getTransaction().begin();
        
        TypedQuery<Usuarios> q =
                em.createNamedQuery("Usuarios.findByRfid",Usuarios.class)
                        .setParameter("rfid", u.getRfid());
        List<Usuarios> usuarios = q.getResultList();
        
        
        em.getTransaction().commit();
        if( usuarios.size() != 1)
            return null;
        return usuarios.get(0);
    }

    private static void validaUsuario(Usuarios u) throws NegocioException {
        if(u==null) 
            throw new NegocioException("Usuário inválido");
        
        if(u.getUsername()==null || u.getUsername().equals(""))
            throw new NegocioException("Nome de Usuário inválido");
        if(u.getPassword()==null || u.getPassword().equals(""))
            throw new NegocioException("Senha não pode ser vazia!");
        if(u.getRfid()==null || u.getRfid().equals(""))
            throw new NegocioException("RFID não pode ser vazio!");
        
        for(Usuarios p : usuarios)
            if(p.getRfid().equals(u.getRfid()))
                throw new NegocioException("RFID em uso");
            else if(p.getUsername().equals(u.getUsername()))
                throw new NegocioException("Nome de Usuário em uso");
            //else if(p.getPhotoPath()==u.getPhotoPath())
            //    throw new NegocioException("RFID em uso");
    }

    
    
}

